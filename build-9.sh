#!/bin/sh -x

set -e

SVERSION=3.3.2
DVERSION=3.3.2
PLATFORM=x86_64-centos9-gcc11-opt

mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../${DVERSION}/${PLATFORM} ../Catch2-${SVERSION}/
make clean
make -j 8
make install
cd ..
